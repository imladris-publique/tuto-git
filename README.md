# Tuto git

Ici vous pouvez trouver les conventions que nous utilisons dans git et également quelques commandes git utiles.

Voici le protocole :

0. Soit on fork un code, soit on crée un repo vide, on ajoute un README.md et une licence si absents.
1. on crée depuis main une branche feature/nom-de-la-fonctionnalité-à-développer ou une branche fix/nom-du-bug-à-réparer.
code : 
```bash
git checkout main
git branch feature/[name]
```

ou

```bash
git checkout main
git branch fix/[name]
```

2. 
On ne bosse que sur cette branche f*/[name] (f* = fix ou feature) jusqu'à avoir un truc à tester.
code:
```bash
git checkout f*/[name]
```
On termine son premier commit par le code :
```bash
git push --set-upstream origin f*/[name]
```

3. on crée une branche test/nom-du-serveur-de-test depuis la branche main et on merge depuis la branche de test.
code:
```bash
git checkout main
git branch test/[name]
git checkout test/[name]
git merge f*/[name]
git add -A
git commit -m ""
git push --set-upstream origin test/[name]
```

4. On passe de la branche f* à la branche test et vice-versa jusqu'à avoir un truc prêt à lancer en prod. Alors on crée une branche prod/nom-du-serveur-de-prod depuis main et on merge test depuis prod.
code :
```bash
git checkout main
git branch prod/[name]
git checkout prod/[name]
git merge test/[name]
git add -A
git commit -m ""
git push --set-upstream origin prod/[name]
```

5. Une fois satisfait de sa petite fonctionnalité au point de vouloir la faire connaître à autrui, on crée un autre repo (publique) ou une branche dans le repo publique existant (de soi ou d'un autre).
code :
```bash
git checkout main
git branch export/[repo]/f*/[name]
git checkout export/[repo]/f*/[name]
git cherry-pich [liste des commits à recopier]
```
ou pour la dernière ligne
```bash
git rebase -i origin/f*/[name]
```
ou quelque chose comme ça.

6. On crée une merge request depuis export/[repo]/f*/[name] vers la branche f*/[name] du repo forké.

submodule et subtree
--------------------

##### git submodule

Pour créer des submodules, il faut faire :

```bash
git submodule add [url_du_repo]
```
où url_du_repo est l’url du repo que l’on souhaite «cloner» dedans.
Les modifs du submodule ne sont pas gitées.

##### git subtree

Pour créer des subtree, il faut faire :

```bash
git subtree -P [name] add [url_du_repo] [branche]
```

où name est le nom du répertoire créé par subtree (le préfixe), url_du_repo comme pour submodule et branche est la branche «clonée» (la ref)

Les modifs du subtree pourront être gitées depuis la racine.

Supprimer un fichier
--------------------
##### Sources du tuto

Pour nettoyer notre arbre git, je me base sur les tutos suivants :
- https://fabpl.github.io/git/2020/07/02/supprimer-fichier.html
- lien mort https://eticweb.info/tutoriels-git/comment-supprimer-les-fichiers-supprimes-de-git/
- https://stackovercoder.fr/programming/8740187/git-how-to-remove-file-from-historical-commit
- https://dev.to/balogh08/cleaning-your-git-history-safely-removing-sensitive-data-10i5

##### Première commande

La commande est :
```bash
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch truc-rigolo.txt' --prune-empty --tag-name-filter cat -- --all
```
en remplaçant **truc-rigolo.txt** par le **chemin** de votre fichier.

Elle agit en local sur toutes les branches et indique les branches modifiées.

Vous devez obtenir un (remplacez **main** par le nom de votre branche) :
```bash
Ref 'refs/heads/main' was rewritten
Ref 'refs/remotes/origin/main' was rewritten
```
##### Warning éventuel

Si au lieu de ça vous obtenez :
```bash
WARNING: Ref 'refs/heads/main' is unchanged
WARNING: Ref 'refs/remotes/origin/main' is unchanged
```
cela peut signifier que vous avez oublié de spécifier le chemin complet de votre fichier.

##### Options supplémentaires (non requis)

On peut éventuellement utiliser aussi les commandes :

```bash
rm -rf .git/refs/original       # careful
git gc --aggressive --prune=now # danger
```
mais normalement ce n'est pas nécessaire et c'est dangereux.

##### Étape supplémentaire pour les branches protégées et main

Attention, si on veut pusher dans main (ou dans une autre branche protégée) il faut penser à **autoriser les push forcés** sur cette branche (pas besoin de la déprotéger en revanche).

##### Dernières étapes

Il faut ensuite pusher chaque branche modifiée une par une avec git push --force.

Si on ne souhaite pas pusher toutes les branches, il suffit de faire un git pull une fois les branches requises pushées.

Il n'enlève pas la référence au fichier du .gitignore.

##### Cas particulier des dossiers

Il buggue avec des dossiers, il faut faire truc-rigolo/* pour effacer le contenu du dossier (et le dossier il me semble). Si un dossier et un fichier portent le même nom, le fichier ne peut pas être supprimé, il faut donc effacer d'abord le dossier avant le fichier.


Supprimer simplement un commit
------------------------------

Pour supprimer un commit :
- https://sebastien-gandossi.fr/blog/git-supprimer-un-commit-local-ou-distant

```bash
git reset --hard HEAD~
git push --force-with-lease
```

### Explications et mises en garde :

Quand on fait
```bash
git checkout HEAD~
```
c’est pareil que
```bash
git reset HEAD~
```
sauf qu'avec reset on annule le pull.

On obtient après cette première commande une mise en garde disant que votre branche locale est en retard d'un commit.

Attention : si vous étiez en train de faire d'autres modifs, pensez à faire plutôt :

```bash
git stash
git reset --hard HEAD~
git push --force-with-lease
git stash pop
```
l’option hard a l’effet du git checkout mon-fichier.txt.

Les modifications ne seront effectives ailleurs qu'en local qu'une fois pushées.
Attention : on peut remplacer le 
```bash
git push --force-with-lease
```
par
```bash
git push --force
```
si on a dit avant à tout le monde "ne push surtout pas tes modifs, attend"

